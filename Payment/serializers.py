from rest_framework import serializers
from django.contrib.auth import get_user_model
from djstripe.models import PaymentMethod, Card


class CardSerializer(serializers.ModelSerializer):

    # card = serializers.JSONField()
    # billing_details = serializers.JSONField()
    class Meta:
        model = Card
        exclude = []
        
class PaymentMethodSerializer(serializers.ModelSerializer):
    card = serializers.JSONField()
    class Meta:
        model = PaymentMethod
        exclude = []