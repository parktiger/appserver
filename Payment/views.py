import stripe
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.models import User
from django.shortcuts import render
from djstripe.models import Customer, PaymentMethod, Card
from djstripe.settings import STRIPE_LIVE_MODE, STRIPE_SECRET_KEY
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED,
                                   HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST,
                                   HTTP_401_UNAUTHORIZED)

from .serializers import CardSerializer, PaymentMethodSerializer

print(STRIPE_LIVE_MODE, STRIPE_SECRET_KEY)
stripe.api_key = STRIPE_SECRET_KEY

# Create your views here.

@api_view(['GET'])
def cards(request):
    user= request.user
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    customer = Customer.objects.filter(subscriber=user).first()
    if not customer:
        customer = Customer.create(subscriber=user)
    # cards = PaymentMethod.objects.filter(type="card", customer=customer)
    # cards = Card.objects.filter(customer=customer)
    cards = customer.payment_methods.all()
    serializer = PaymentMethodSerializer(cards, many=True)
    # serializer = CardSerializer(cards, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['POST'])
def add_card(request):
    user= request.user
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    customer = Customer.objects.filter(subscriber=user).first()
    if not customer:
        customer = Customer.create(subscriber=user)
    source = request.data.get('tokenId', None)
    # cards = PaymentMethod.objects.filter(type="card", customer=customer)
    try:
        new_source = customer.add_payment_method(source)
        cards = customer.payment_methods.all()
        serializer = PaymentMethodSerializer(cards, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Exception as e:
        return Response(e.error, status=status.HTTP_406_NOT_ACCEPTABLE)
    cards = Card.objects.filter(customer=customer)
    all_serializer = CardSerializer(cards, many=True, context={'request':request})
    card_list = all_serializer.data
    try:
        # customer.add_payment_method(source)
        new_card = customer.add_card(source)
        new_card_object = {
            "brand": new_card.brand,
            "last4": new_card.last4,
            "exp_month": new_card.exp_month,
            "exp_year": new_card.exp_year,
            "id": new_card.id
        }
        card_list.append(new_card_object)
        return Response(card_list, status=status.HTTP_200_OK)
    except:
        pass
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def card_delete(request, card_id):
    user = request.user
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    customer = Customer.objects.get(subscriber=user)
    try:
        # customer.
        card = PaymentMethod.objects.get(customer=customer, id=card_id)
        # card = Card.objects.get(customer=customer, id=card_id)
    except:
        card = None
    if card:
        card.detach()
        return Response(status=status.HTTP_204_NO_CONTENT)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def charge(request):
    user= request.user
    source = request.data.get('source', None)
    amount = request.data.get('amount', None)
    currency = request.data.get('currency', None)
    email = request.data.get('email', None)
    customer = None
    user = request.user
    if request.user.is_authenticated:
        customer = Customer.objects.filter(subscriber=user).first()
        if not customer:
            customer = Customer.create(subscriber=user)
    try:
        if customer:
            """ OLD CHARGE METHOD FOR AUTHENTICATED USERS """
            # resp = stripe.Charge.create(
            #     amount=amount,
            #     currency=currency,
            #     source=source,
            #     customer=customer.id
            # )
            """ NEW CHARGE METHOD FOR AUTHENTICATED USERS """
            if source.startswith('pm_'):
                resp = stripe.PaymentIntent.create(
                    amount=amount,
                    currency=currency,
                    payment_method_types=["card"],
                    confirm=True,
                    receipt_email=email,
                    # payment_method=source,
                    setup_future_usage='off_session',
                    customer=customer.id,
                    payment_method=source
                )
            else:
                resp = stripe.PaymentIntent.create(
                    amount=amount,
                    currency=currency,
                    payment_method_types=["card"],
                    confirm=True,
                    receipt_email=email,
                    # payment_method=source,
                    setup_future_usage='off_session',
                    customer=customer.id,
                    payment_method_data={
                        "type": "card",
                        "card": {
                            "token": source
                        }
                    }
                )
        else:
            """ OLD CHARGE METHOD FOR ANANOYMOUS USERS """
            # resp = stripe.Charge.create(
            #     amount=amount,
            #     currency=currency,
            #     source=source,
            #     receipt_email=email
            # )
            """ NEW CHARGE METHOD FOR ANANOYMOUS USERS """
            resp = stripe.PaymentIntent.create(
                amount=amount,
                currency=currency,
                payment_method_types=["card"],
                confirm=True,
                receipt_email=email,
                # payment_method=source,
                # setup_future_usage='off_session',
                # customer=customerId,
                payment_method_data={
                    "type": "card",
                    "card": {
                        "token": source
                    }
                }
            )
        return Response(resp, status=status.HTTP_200_OK)
    except Exception as e:
        return Response(e.error, status=status.HTTP_400_BAD_REQUEST)
    
# {"source":"card_1HArxADlSvGwyJU4lZ6lCCtG","amount":10,"currency":"usd","description":""}
