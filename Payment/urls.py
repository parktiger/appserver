"""ParkTiger URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from .views import cards, add_card, card_delete, charge
urlpatterns = [
    path('cards/', cards, name="payment_cards"),
    path('cards/add/', add_card, name="payment_cards_add"),
    path('cards/<str:card_id>/', card_delete, name="payment_vehicle_delete"),
    path('charge/', charge, name="payment_charge")
]
