from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Feedback(models.Model):

    ANGRY = 'Angry'
    SAD = 'Sad'
    NEUTRAL = 'Neutral'
    SATISFIED = 'Satisfied'
    HAPPY = 'Happy'

    RATING_CHOICES = (
        (1, ANGRY),
        (2, SAD),
        (3, NEUTRAL),
        (4, SATISFIED),
        (5, HAPPY),
    )

    rating = models.CharField(max_length=1, choices=RATING_CHOICES)
    user_email = models.EmailField(blank=True, null=True, default="")
    message = models.TextField(blank=True, default="")
    screenshot = models.ImageField(blank=True, null=True, upload_to='feedback')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Feedback"
        verbose_name_plural = "Feedbacks"

    def __str__(self):
        return "%s at %s" % (str(self.user_email) or 'Someone', self.created_at.strftime('%d-%m-%Y %H:%M'))


class Support(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    message = models.TextField(blank=True, default="")
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "Support Message"
        verbose_name_plural = "Support Messages"
        
    def __str__(self):
        return "Support Request from %s at %s" % (str(self.user), self.created_at.strftime('%d-%m-%Y %H:%M'))
