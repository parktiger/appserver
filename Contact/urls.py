"""ParkTiger URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from .views import feedback, support

urlpatterns = [
    path('feedback/', feedback, name="contact_feedback"),
    path('support/', support, name="contact_support"),
]
