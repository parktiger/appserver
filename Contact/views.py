from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.status import (HTTP_201_CREATED, HTTP_400_BAD_REQUEST,
                                   HTTP_401_UNAUTHORIZED)

from .serializers import FeedbackSerializer, SupportSerializer
from django.core.mail import send_mail
from ParkTiger.settings import SENDER_EMAIL, ADMIN_MAILS
# Create your views here.

@api_view(['POST'])
def feedback(request):
    user = request.user
    feedback_request = FeedbackSerializer(data=request.data)
    if feedback_request.is_valid():
        ticket = feedback_request.save()
        if request.user.is_authenticated:
            ticket.user_email = user.username
            ticket.save()
        # TODO: Provide link to admin in mail.
        try:
            message = 'New feedback by ' + str(ticket) + ' \n\nMessage: ' + ticket.message + \
                '\n\nRating: ' + str(ticket.rating) + '/5\n\n'
            if ticket.screenshot and ticket.screenshot.url:
                message = message + 'Attachment: ' + request.build_absolute_uri(ticket.screenshot.url)
            send_mail('New Feedback', message, SENDER_EMAIL, ADMIN_MAILS, fail_silently=False)
        except:
            return Response(status=HTTP_400_BAD_REQUEST)
        return Response(status=HTTP_201_CREATED)
    return Response(status=HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def support(request):
    user = request.user
    if not request.user.is_authenticated:
        return Response(status=HTTP_401_UNAUTHORIZED)
    support_request = SupportSerializer(data=request.data)
    if support_request.is_valid():
        ticket = support_request.save()
        ticket.user = user
        ticket.save()
        try:
            message = str(ticket) + '\n\nMessage: ' + \
                ticket.message
            send_mail('New Support Request', message, SENDER_EMAIL, ADMIN_MAILS, fail_silently=False)
        except:
            return Response(status=HTTP_400_BAD_REQUEST)
        return Response(status=HTTP_201_CREATED)
    return Response(status=HTTP_400_BAD_REQUEST)
