from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Vehicle, Resource

class UserSerializer(serializers.HyperlinkedModelSerializer):
    full_name = serializers.SerializerMethodField()
    phone_prefix = serializers.CharField(source="profile.phone_prefix", allow_blank=True, allow_null=True, read_only=True)
    phone_number = serializers.CharField(source="profile.phone_number", allow_blank=True, allow_null=True, read_only=True)
    country = serializers.CharField(source="profile.country", allow_blank=True, allow_null=True, read_only=True)
    def get_full_name(self, obj):
        name = obj.get_full_name()
        return name if name.strip() else "No Name"

    class Meta:
        model = User
        fields = ['id', 'username', 'first_name', 'last_name', 'full_name', 'phone_prefix', 'phone_number', 'country']
        read_only_fields = ('id', 'username', 'full_name', 'phone_prefix', 'phone_number', 'country')


class VehicleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vehicle
        fields = ['id', 'license_no', 'model_name']
        read_only_fields = ['id']

class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = '__all__'
        read_only_fields = ['id']
