from django.db import models
from django.contrib.auth.models import User
import jsonfield
# Create your models here.
class Vehicle(models.Model):

    license_no = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        verbose_name = "Vehicle"
        verbose_name_plural = "Vehicles"

    def __str__(self):
        return "%s - %s - %s" % (str(self.user), self.model_name, self.license_no)


class OTP(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    otp = models.CharField(max_length=20, default="", blank=True)
    last_updated = models.DateTimeField(auto_now=True, blank=True)
    
    class Meta:
        verbose_name = "OTP"
        verbose_name_plural = "OTPs"
        
    def __str__(self):
        return "%s" % (str(self.user))
    
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    phone_prefix = models.CharField(max_length=10, blank=True, null=True, default="43")
    country = models.CharField(max_length=10, blank=True, null=True, default='AT')
    phone_number = models.CharField(max_length=20, blank=True, null=True, default="")
    
    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"
        
    def __str__(self):
        return "%s" % (str(self.user))

class Resource(models.Model):
    data = jsonfield.JSONField(null=False, blank = False)
    cover = models.ImageField(null=False, blank=False)

    class Meta:
        verbose_name = 'Resource'
        verbose_name_plural = 'Resources'

    def __str__(self):
        return "%s" % str(self.id)
