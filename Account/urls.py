"""ParkTiger URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from .views import login, register, profile, vehicles, \
    vehicle, vehicle_delete, change_password, social_login, \
    request_reset_password, validate_reset_request, \
    reset_password, expiring_today, resources, profile_deactivate

urlpatterns = [
    path('login/', login, name="accounts_login"),
    path('social_login/', social_login, name="accounts_social_login"),
    path('register/', register, name="accounts_register"),
    path('change_password/', change_password, name="change_password"),
    path('request_reset_password/', request_reset_password, name='request_reset_password'),
    path('validate_reset_request/', validate_reset_request, name='validate_reset_request'),
    path('reset_password/', reset_password, name='reset_password'),
    path('profile/', profile, name="accounts_profile"),
    path('vehicles/', vehicles, name="accounts_vehicles"),
    path('vehicle/', vehicle, name="accounts_vehicle"),
    path('vehicle/<int:vehicle_id>/', vehicle, name="accounts_vehicle"),
    path('vehicle/delete/<int:vehicle_id>/', vehicle_delete, name="accounts_vehicle_delete"),
    path('check_expiring_today', expiring_today, name="accounts_expiring_today"),
    path('resources/', resources, name="accounts_resources"),
    path('profile_deactivate/', profile_deactivate, name="accounts_profile_deactivate")
]
