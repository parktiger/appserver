import requests
import hashlib
import json
from datetime import datetime, timedelta
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.shortcuts import render, get_object_or_404
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import (HTTP_200_OK, HTTP_201_CREATED,
                                   HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST,
                                   HTTP_401_UNAUTHORIZED)

from .models import Vehicle, OTP, Profile, Resource
from .serializers import UserSerializer, VehicleSerializer, ResourceSerializer
from random import randint
from django.core.mail import send_mail
from ParkTiger.settings import SENDER_EMAIL, ADMIN_MAILS
from datetime import timedelta
from django.utils import timezone
from push_notifications.models import APNSDevice, GCMDevice
# Create your views here.

@api_view(['POST'])
def login(request):
    data = request.data
    if data.get('email') and data.get('password'):
        user = authenticate(
            request,
            username=data.get('email'), 
            password=data.get('password'))
        if not user: 
            return Response(status.HTTP_400_BAD_REQUEST)
        serializer = UserSerializer(user)
        response = serializer.data
        token, created = Token.objects.get_or_create(user=user)
        response['token'] = token.key
        return Response(response, status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def social_login(request):
    data = request.data
    if data.get('email'):
        user, created = User.objects.get_or_create(username=data.get('email'))
        if created:
            user.set_unusable_password()
        f_name = data.get("first_name", "")
        l_name = data.get("last_name", "")
        if f_name:
            user.first_name = f_name
        if l_name:
            user.last_name = l_name
        user.save()
        Profile.objects.get_or_create(user=user)
        token, c = Token.objects.get_or_create(user=user)
        serializer = UserSerializer(user)
        response = serializer.data
        response['token'] = token.key
        return Response(response, status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def register(request):
    data = request.data
    if data.get('email') and data.get('password'):
        user, created = User.objects.get_or_create(username=data.get('email'))
        if created:
            user.set_password(data.get('password'))
            if data.get('first_name', None):
                user.first_name = data.get('first_name', '')
            if data.get('last_name', None):
                user.last_name = data.get('last_name', '')
            user.save()
            profile = Profile.objects.create(user=user)
            if data.get('phone_prefix', None):
                profile.phone_prefix = data.get('phone_prefix', '43')
            if data.get('phone_number', None):
                profile.phone_number = data.get('phone_number', '')
            if data.get('country', None):
                profile.country = data.get('country', 'AT')
            profile.save()
            token = Token.objects.create(user=user)
            serializer = UserSerializer(user)
            response = serializer.data
            response['token'] = token.key
            return Response(response, status.HTTP_201_CREATED)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def profile(request):
    user = request.user
    if not user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    serializer = UserSerializer(user)
    if request.method == 'POST':
        serializer = UserSerializer(user, data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        user = serializer.save()
        profile, c = Profile.objects.get_or_create(user=user)
        profile.phone_prefix = request.data.get("phone_prefix", "43")
        profile.phone_number = request.data.get("phone_number", "")
        profile.country = request.data.get("country", "AT")
        profile.save()
    try:
        token = Token.objects.get(user=user).key
    except:
        token = ''
    response = serializer.data
    response['token'] = token
    return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def vehicles(request):
    user= request.user
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    vehicles = Vehicle.objects.filter(user=user)
    serializer = VehicleSerializer(vehicles, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)

@api_view(['POST'])
def vehicle(request, vehicle_id=None):
    user = request.user
    response_status = status.HTTP_200_OK
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    try:
        vehicle = Vehicle.objects.get(user=user, id=vehicle_id)
    except:
        vehicle = None
    if vehicle_id is not None and vehicle is None:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    serializer = VehicleSerializer(vehicle, data=request.data)
    if not serializer.is_valid():
        return Response(status=status.HTTP_400_BAD_REQUEST)
    new_vehicle = serializer.save()
    
    if not vehicle:
        response_status = status.HTTP_201_CREATED
        new_vehicle.user=user
        new_vehicle.save()
    vehicles = Vehicle.objects.filter(user=user)
    all_serializer = VehicleSerializer(vehicles, many=True)
    return Response(all_serializer.data, status=response_status)


@api_view(['POST'])
def vehicle_delete(request, vehicle_id):
    user = request.user
    try:
        vehicle = Vehicle.objects.get(user=user, id=vehicle_id)
    except:
        vehicle = None
    if vehicle:
        vehicle.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def change_password(request):
    user = request.user
    old_password = request.data.get("old_password", None)
    new_password = request.data.get("new_password", None)
    if not user.is_authenticated or not old_password or not new_password or not user.check_password(old_password):
        return Response(status=status.HTTP_400_BAD_REQUEST)
    user.set_password(new_password)
    return Response(status=status.HTTP_200_OK)

@api_view(['POST'])
def request_reset_password(request):
    email = request.data.get("email", None)
    resend = request.data.get("resend", False)
    user = get_object_or_404(User, username=email)
    if user and user.is_active:
        otp, created = OTP.objects.get_or_create(user=user)
        if created or not resend:
            otp.otp = str(randint(1,9999)).zfill(4)
        otp.save()
        # Send email
        try:
            message = "{0}\n\n {1} \n\n {2} \n\n {3}\n\n {4}".format(
                "Lieber Kunde, ",
                "Der Code zum Zurücksetzen Ihres Passworts lautet: ",
                otp.otp,
                "Dieser Code ist die nächsten 2 Stunden gültig.",
                "Liebe Grüße,\nParktiger"
            )
            send_mail(
                'Parktiger - Passwort zurücksetzen',
                message,
                SENDER_EMAIL,
                [user.username],
                fail_silently=False)
        except:
            return Response(status=HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['POST'])
def validate_reset_request(request):
    otpToken = request.data.get("otp", None)
    otp = get_object_or_404(OTP, otp=otpToken)
    if otp and otp.last_updated + timedelta(hours=2) > timezone.now():
        return Response(status=status.HTTP_200_OK)
    else:
        otp.delete()
    return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
def reset_password(request):
    otpToken = request.data.get("otp", None)
    password = request.data.get("password", None)
    otp = get_object_or_404(OTP, otp=otpToken)
    if otp and password:
        user = otp.user
        user.set_password(password)
        user.save()
        token, created = Token.objects.get_or_create(user=user)
        serializer = UserSerializer(user)
        response = serializer.data
        response['token'] = token.key
        otp.delete()
        return Response(response, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)

SITE_ID = '31575'
PLANYO_API_KEY = 'd44154b18a75c583397c2d5fd00714160eb13a86c0599fb273dbb527c14137'
PLANYO_HASH_KEY = 'Hbcc3fd7d6187b18bfcf7578ccc3d9a1dbd6e6fd459f8d1129ede9b5e8c5c69'

def generateHashKey(method, time):
    fullString = PLANYO_HASH_KEY + time + method
    aa = hashlib.md5(fullString.encode('utf-8'))
    return aa.hexdigest()

def get_hash_data(method):
    hash_timestamp = "%.3f" % (timezone.now().timestamp())
    hash_key = generateHashKey(method, hash_timestamp)
    return {
        "hash_timestamp": hash_timestamp,
        "hash_key": hash_key,
        "method": method,
    }

@api_view(['GET'])
def expiring_today(request):
    results = check_expiring_today(True)
    return Response(results, status=status.HTTP_200_OK)

def check_expiring_today(should_return=False):
    users = User.objects.all()
    url = 'https://www.planyo.com/rest/'
    method = "list_reservations"
    today = timezone.now().today() + timedelta(days=1)
    start = datetime(today.year, today.month, today.day)
    payload = {
        "end_time": "%.0f" % start.timestamp(),
        "start_time": "%.0f" % (start.timestamp() - 1000000000),
        "site_id": SITE_ID,
        "list_by_creation_date": False,
        "sort": "end_time",
        "sort_reverse": True,
        "detail_level": 63,
        "api_key": PLANYO_API_KEY,
        # "must_include_time_range_end": True
    }
    hash_data = get_hash_data(method);
    payload.update(hash_data)
    results = []
    for user in users:
        print(user.username)
        payload.update({
            "user_email": user.username
        })
        response = requests.get(url, params=payload)
        data = response.json()
        td = start - timedelta(days=1)
        i = 0
        print(data)
        ddata = data.get('data',None)
        if ddata:
            ress = ddata.get('results', [])
            if ress:
                for result in ddata.get('results', []):
                    e_date = datetime.strptime(result['end_time'].split(' ')[0], "%Y-%m-%d")
                    if(e_date == td):
                        print(i)
                        results.append({
                            # "user": user,
                            "result": result
                        })
                        device = APNSDevice.objects.filter(user=user)
                        # message = "Body"
                        extra = {
                            "title": result.get('name', "Parkplatzreservierung"),
                            "message": "Ihre Reservierung läuft heute ab",
                            "big_text": "Ihre Reservierung bei %s läuft heute ab" % (result.get('name', 'Parkplatz')),
                            "reservation": result,
                            "android": {
                                "bigText": "Ihre Reservierung bei %s läuft heute ab" % (result.get('name', 'Parkplatz')),
                            }
                        }
                        device = APNSDevice.objects.all()
                        rr = device.send_message(None, extra = extra)
                        print(rr)
                        device = GCMDevice.objects.filter(user=user)
                        device = GCMDevice.objects.all()
                        rr = device.send_message(None, extra = extra)
                        print(rr)
                    i = i+ 1
        print(len(results))
        if should_return:
            return results
        # TODO: For each item in results identify device and send it a push notification


@api_view(['GET'])
def resources(request):
    resources = Resource.objects.all()     
    serializer = ResourceSerializer(resources, many=True, context={'request': request})
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
def profile_deactivate(request):
    if not request.user.is_authenticated:
        return Response(status=status.HTTP_401_UNAUTHORIZED)
    user = request.user
    user.delete()
    return Response(status = status.HTTP_200_OK)